from fastapi import APIRouter, HTTPException
from app.api import api
from app.model.Message import Message
import logging
import random

logger = logging.getLogger(__name__)
router = APIRouter()


@router.get("/hello")
def hello(name: str):
    """
    This function is an endpoint for the GET request at the "/hello" route. It takes in a parameter name of type str, representing the name of the person to greet. It returns a JSON response with a "message" field containing a greeting message in the format "Hello {name}!".

    Parameters:

        name (str): The name of the person to greet.

    Returns:

        dict: A JSON response containing a greeting message.

    Example:

        GET /hello?name=John

    Response:

        {"message": "Hello John!"}
    """
    logger.info("called 'hello' endpoint")
    return {"message": f"Hello {name}!"}


@router.get(
    "/message",
    summary="Random Nation Eduation (NE) Message",
    response_model=Message,
)
def get_message():
    """
    This function is an endpoint for the GET request at the "/message" route. It will returns one random NE message.

    Parameters:

        name (str): The name of the person to greet.

    Returns:

        dict: A JSON response containing a greeting message.

    Example:

        GET /message
    """
    logger.info("called 'message' endpoint")
    messages = api.get_messages()
    return random.choice(messages)


@router.get("/location",
            summary="Get the details of firework site nearest to you")
def nearest_fireworks_site(station):
    """
    This function is an endpoint for the GET request at the "/location" route. It will return the location and details where there will fireworks.
    Parameters:

        station (str): The name of MRT station

    Returns:

        dict: A JSON response containing the details of the nearest firework site.

    Example:

        GET /location?station=pioneer
    """
    if not station:
        raise HTTPException(status_code=400,
                            detail="Please enter valid string")
    if not station.istitle():
        raise HTTPException(
            status_code=400,
            detail="Station name must be in title case (e.g., 'Pioneer')")
    return api.nearest_firework_site(station)


@router.get("/flag")
def flag():
    """
    This function is an endpoint for the GET request at the "/flag" route.

    Returns:
    str: The flag obtained from the "api.print_secret()" function.
    """
    return api.print_secret()
